import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumDetailsComponent } from './containers/album-details/album-details.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';

const routes: Routes = [
  {
    path: 'search',
    component: AlbumSearchComponent
  },
  {
    path: 'search/album/:album_id',
    component: AlbumDetailsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})  
export class MusicSearchRoutingModule { }
