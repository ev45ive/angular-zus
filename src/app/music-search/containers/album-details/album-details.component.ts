import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EMPTY, iif } from 'rxjs';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { Track } from 'src/app/core/model/Album';
import { MusicApiService } from 'src/app/core/services/music-api/music-api.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  album_id = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('album_id'))
  )

  album = this.album_id.pipe(
    switchMap(album_id =>
      iif(() => !!album_id,
        this.service.fetchAlbumById(album_id!),
        EMPTY)),
    shareReplay()
  )

  currentTrack?: Track

  @ViewChild('audioRef', { read: ElementRef, static: false })
  audioRef?: ElementRef<HTMLAudioElement>

  constructor(
    private service: MusicApiService,
    private route: ActivatedRoute) {
  }


  playTrack(track: Track) {
    this.currentTrack = track
    setTimeout(() => {
      if (this.audioRef?.nativeElement) {
        this.audioRef.nativeElement.volume = 0.1;
        this.audioRef.nativeElement.play()
      }
    })
  }

  ngOnInit(): void {
  }

}
