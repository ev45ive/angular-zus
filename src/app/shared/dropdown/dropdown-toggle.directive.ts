import { Directive, HostBinding, HostListener, Input, Optional } from '@angular/core';
import { DropdownMenuDirective } from './dropdown-menu.directive';
import { DropdownDirective } from './dropdown.directive';

@Directive({
  selector: '[appDropdownToggle]'
})
export class DropdownToggleDirective {

  @Input('appDropdownToggle') menu: DropdownMenuDirective | '' = '';

  constructor(
    
    @Optional() private parent: DropdownDirective | null) {
  }

  ngOnInit(): void {
    if (this.parent?.menu && !this.menu) {
      this.menu = this.parent?.menu!
    }
  }

  @HostBinding('attr.aria-haspopup')
  hasPopup = true

  @HostBinding('attr.aria-expanded')
  isOpen = false

  @HostListener('click')
  toggleClicked() {
    if (this.menu) {
      this.menu?.toggle()
      this.isOpen = this.menu?.isOpen!
    }
  }
}
