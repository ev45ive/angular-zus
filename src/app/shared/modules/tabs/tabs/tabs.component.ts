import { Component, OnInit } from '@angular/core';
import { TabComponent } from '../tab/tab.component';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  children: TabComponent[] = []

  toggle(activeTab: TabComponent) {
    this.children.forEach(tab => {
      if(activeTab === tab){
        tab.isOpen = true
      }else{
        tab.isOpen = false
      }
    })
  }

  unregisterTab(tab: TabComponent) {
    const index = this.children.indexOf(tab)
    this.children.splice(index, 1)
  }
  registerTab(tab: TabComponent) {
    this.children.push(tab)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
