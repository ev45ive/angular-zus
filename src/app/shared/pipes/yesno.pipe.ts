import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno',
  // pure: false, // disable cache
})
export class YesnoPipe implements PipeTransform {

  transform(value: boolean, yes = 'Yes', no = 'No'): string {
    // console.log('transform')
    return value ? yes : no
  }

}
