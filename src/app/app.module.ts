import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { MusicSearchModule } from './music-search/music-search.module';
import { MocksModule } from './mocks/mocks.module';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    // PlaylistsModule,
    CoreModule,
    SharedModule,
    MusicSearchModule,
    !environment.production ? MocksModule : [],
    AppRoutingModule,
  ],
  providers: [
    // {
    //   provide: 'INITIAL_ALBUMS',
    //   useValue: mockAlbums
    // }
  ],
  bootstrap: [AppComponent/* SidebarComponent, PopUpComponent,... */]
})
export class AppModule { }
// export class AppModule implements DoBootstrap{

//   ngDoBootstrap(appRef: ApplicationRef): void {
//     // getconfigfromserer().then(  config => ...
//     setTimeout(()=>{
//       appRef.bootstrap(AppComponent,'app-root')
//     },2000)
//   }
// }
