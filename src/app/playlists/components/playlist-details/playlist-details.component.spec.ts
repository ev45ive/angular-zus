import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CardComponent } from 'src/app/shared/components/card/card.component';
import { YesnoPipe } from 'src/app/shared/pipes/yesno.pipe';

import { PlaylistDetailsComponent } from './playlist-details.component';

fdescribe('PlaylistDetailsComponent', () => {
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;
  let debugElem: DebugElement

  //  Error: Timeout - Async function did not complete within 5000ms (set by jasmine.DEFAULT_TIMEOUT_INTERVAL)
  beforeEach(async (/* done */) => {
    await TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent, YesnoPipe, CardComponent],
      imports: [],
      providers: []
    }).compileComponents()//.then(done)
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent)
    component = fixture.componentInstance
    debugElem = fixture.debugElement
    component.playlist = {
      id: '123', name: 'Placki', description: 'desc', public: false
    }
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).not.toBeUndefined()
    // throw 'ups...'
    // expect(new PlaylistDetailsComponent()).not.toBeUndefined()
  })

  it('should render playlist details', () => {
    const elem = debugElem.query(By.css('.card-title')).nativeElement
    expect(elem.textContent).toBe('Placki');
  });

  it('should render playlist details', () => {
    const elem = debugElem.query(By.css('.public_private')).nativeElement
    component.playlist.public = false
    fixture.detectChanges()
    expect(elem.textContent).toMatch('NOPE');
    
    component.playlist.public = true
    fixture.detectChanges()
    expect(elem.textContent).toMatch('YEAH!');
  });

  it('should emit edit event on edit button', () => {
    const spy = jasmine.createSpy()
    component.edit.subscribe(spy)
    
    const elem = debugElem.query(By.css('button')).nativeElement
    elem.click()
    expect(spy).toHaveBeenCalledTimes(1)
  });
  

});

// it('shoud test', () => {
//   expect(true).toBe(true)
// })

// let component: PlaylistDetailsComponent;
// let fixture: ComponentFixture<PlaylistDetailsComponent>;

// beforeEach(async () => {
//   await TestBed.configureTestingModule({
//     declarations: [ PlaylistDetailsComponent ]
//   })
//   .compileComponents();
// });

// beforeEach(() => {
//   fixture = TestBed.createComponent(PlaylistDetailsComponent);
//   component = fixture.componentInstance;
//   fixture.detectChanges();
// });

// it('should create', () => {
//   expect(component).toBeTruthy();
// });