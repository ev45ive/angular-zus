import { Component, DoCheck, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Playlist } from 'src/app/core/model/Playlist';

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnChanges, OnInit, DoCheck, OnDestroy {

  @Input() playlist!: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  cancelClick() { this.cancel.emit() }

  saveClick(form: NgForm) {
    if (form.invalid) { return }
    
    const draft = ({
      ...this.playlist,
      ...form.value
    })
    this.save.emit(draft)
  }

  constructor() {
    // console.log('constructor')
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // this.draft = { ...this.playlist }
    // console.log('ngOnChanges', changes)
  }

  ngDoCheck(): void {
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
    // console.log('ngDoCheck')
  }

  ngOnInit(): void {
    // this.draft = Object.assign({},this.playlist)
    // console.log('ngOnInit')
    // this;
    // debugger
  }

  @ViewChild('formRef', { read: NgForm })
  formRef?: NgForm

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    setTimeout(() => {
      // console.log(this.formRef?.controls.name)

      // this.formRef!.controls.name.setValidators([ ])
    })
  }

  reset() {
    this.formRef?.setValue({
      name: this.playlist.name,
      public: this.playlist.public,
      description: this.playlist.description,
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // console.log('ngOnDestroy')
  }

}
