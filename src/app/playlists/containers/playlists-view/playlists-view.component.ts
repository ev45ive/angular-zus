import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EMPTY, iif, Subject } from 'rxjs';
import { map, shareReplay, startWith, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { Playlist } from 'src/app/core/model/Playlist';
import { PlaylistsApiService } from 'src/app/core/services/playlists-api/playlists-api.service';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: 'details' | 'edit' | 'create' = 'details'
  message = ''

  playlists = this.playlistsService.fetchPlaylists()

  playlist_id = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('playlist_id'))
  )

  currentPlaylist = this.makePlaylistsFetcher()

  emptyPlaylist: Playlist = {
    id: '', name: '', public: false, description: ''
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsApiService) { }

  private makePlaylistsFetcher() {
    return this.playlist_id.pipe(switchMap(id => iif(() => !!id,
      this.playlistsService.fetchPlaylistById(id!),
      EMPTY
    )), shareReplay());
  }

  selectPlaylist(id: Playlist['id']) {
    this.playlist_id.pipe(
      take(1),
    ).subscribe((old_id) => {
      this.router.navigate(['/playlists']
        .concat(id !== old_id ? [id] : []), {})
    })
  }

  removePlaylist(id: Playlist['id']) {
    this.playlistsService.deletePlaylist(id).pipe(
      switchMap(() => this.playlist_id)
    ).subscribe((old_id) => {
      this.playlists = this.playlistsService.fetchPlaylists()
      // If deleted currently selected playlist, navigate to list (unselect)
      if (old_id === id) {
        this.router.navigate(['/playlists'])
      }
    })
  }

  editMode() {
    this.mode = 'edit'
  }

  detailsMode() {
    this.mode = 'details'
  }

  createNewPlaylist() {
    this.mode = 'create'
  }

  save(draft: Playlist) {
    this.playlistsService.savePlaylist(draft)
      .subscribe(saved => {
        this.playlists = this.playlistsService.fetchPlaylists()
          .pipe(tap(() => {
            this.selectPlaylist(saved.id)
            this.currentPlaylist = this.makePlaylistsFetcher()
          }))
        this.mode = 'details'
      })
  }

  saveNew(draft: Playlist) {
    this.playlistsService.savePlaylist(draft)
      .subscribe(saved => {
        this.playlists = this.playlistsService.fetchPlaylists()
          .pipe(tap(() => {
            this.selectPlaylist(saved.id)
            this.currentPlaylist = this.makePlaylistsFetcher()
          }))
        this.mode = 'details'
      })
  }



  ngOnInit(): void {
  }

}
