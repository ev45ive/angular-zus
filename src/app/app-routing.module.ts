import { Inject, NgModule } from '@angular/core';
import { RouterModule, ROUTES, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  },
  {
    path: 'playlists',
    loadChildren: () => import('./playlists/playlists.module')
      .then(m => m.PlaylistsModule)
  },
  {
    path: '**',
    // component: PageNotFoundComponent
    redirectTo: 'search',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // enableTracing: true
    // useHash: true // false => history.pushState
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {

  constructor(@Inject(ROUTES) routes: Routes[]) {
    console.log(routes)
  }
}
