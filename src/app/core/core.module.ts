import { Inject, LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { INITIAL_ALBUMS, API_URL } from './tokens';
import { HttpClient, HttpClientModule, HttpClientXsrfModule, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptor } from './auth/auth.interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // HttpClientXsrfModule.withOptions({})
    HttpClientXsrfModule.disable(),
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    // { provide: LOCALE_ID, useValue: 'en-US' },
    { provide: LOCALE_ID, useValue: 'pl' },
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    // {
    //   provide: HttpClient,
    //   useClass: MySpecialAuthHttpClient
    // },
    // {
    //   provide: AbstractApiService,
    //   useValue: new MusicApiService()
    // }
    // {
    //   provide: MusicApiService,
    //   useFactory(initial: Album[], api_url: string) {
    //     return new MusicApiService(initial, api_url)
    //   },
    //   deps: [INITIAL_ALBUMS, SEARCH_API_URL]
    // },
    // {
    //   provide: MusicApiService,
    //   useClass: MusicApiService,
    //   // deps: [INITIAL_ALBUMS, SEARCH_API_URL]
    // },
    // MusicApiService,
  ]
})
export class CoreModule {

  constructor(private auth: AuthService,
    @Inject(HTTP_INTERCEPTORS) interceptors: HttpInterceptor[]) {
    this.auth.init()
    console.log(interceptors)
  }

}
