import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { from, Observable, of, throwError } from 'rxjs';
import { catchError, map, mergeAll, mergeMap } from 'rxjs/operators';
import { AuthService } from '../services/auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {


  constructor(private auth: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const authRequest = this.authorizeRequest(request)

    return next.handle(authRequest).pipe(
      catchError(error => {
        console.error(error)
        if (!(error instanceof HttpErrorResponse)) {
          return throwError(new Error('Unexpected error'))
        }

        if (error.status === 401) {
          return from(this.auth.login()).pipe(
            mergeMap(() => next.handle(this.authorizeRequest(request))),
          )
        }
        return throwError(new Error(error.error.error.message))
      })
    )
  }

  private authorizeRequest(request: HttpRequest<unknown>) {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    });
  }
}