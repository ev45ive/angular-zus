import { Inject, Injectable } from '@angular/core';
import { Album, SearchAlbumsResponse } from '../../model/Album';
import { INITIAL_ALBUMS, API_URL } from '../../tokens';

import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, EMPTY, ReplaySubject } from 'rxjs';
import { catchError, concatAll, concatMap, exhaust, exhaustMap, map, mergeAll, mergeMap, switchAll, switchMap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class MusicApiService {
  /* Outputs */
  private query = new ReplaySubject<string>(1)
  queryChange = this.query.asObservable()

  private results = new BehaviorSubject<Album[]>(this.initial)
  resultsChange = this.results.asObservable()

  private messages = new ReplaySubject<string>(3)
  messagesChange = this.messages.asObservable()

  constructor(
    @Inject(INITIAL_ALBUMS) private initial: Album[] = [],
    @Inject(API_URL) private api_url: string,
    private http: HttpClient) {

    (window as any).subject = this.results;

    this.query.pipe(
      switchMap(query => this.fetchSearchAlbums(query)),
    ).subscribe(this.results)
  }

  /* Inputs */
  searchAlbums(query: string) {
    this.query.next(query)
  }

  fetchSearchAlbums(query: string) {
    return this.http.get<SearchAlbumsResponse>(
      `${this.api_url}/search`, {
      params: {
        type: 'album',
        query: query
      },
    })
      .pipe(
        map(resp => resp.albums.items),
        catchError((err) => { this.messages.next(err.message); return EMPTY; })
      );
  }

  fetchAlbumById(album_id: string) {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${album_id}`)
  }
}