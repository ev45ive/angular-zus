import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, EMPTY, iif, Observable, of } from 'rxjs';
import { catchError, exhaustMap, filter, switchMap, take } from 'rxjs/operators';
import { UserProfile } from '../../model/User';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersApiService {

  private user = new BehaviorSubject<UserProfile | null>(null)
  userChange = this.user.asObservable()

  constructor(private http: HttpClient, private auth: AuthService) { }

  getCurrentUser(): Observable<UserProfile | null> {
    if (!this.user.getValue()) {
      this.auth.isLoggedIn.pipe(
        switchMap(loggedIn => iif(() => !!loggedIn, this.fetchUser(), of(null)))
      ).subscribe(this.user)
    }
    return this.userChange
  }

  fetchUser = () => this.http.get<UserProfile>(`https://api.spotify.com/v1/me`)
    .pipe(
      catchError(() => EMPTY)
    )
}
