import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { iif, Observable, of } from 'rxjs';
import { exhaustMap, map, mapTo } from 'rxjs/operators';
import { PagingObject } from '../../model/Album';
import { Playlist } from '../../model/Playlist';
import { API_URL } from '../../tokens';
import { UsersApiService } from '../users-api/users-api.service';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsApiService {

  constructor(
    @Inject(API_URL) private api_url: string,
    private userApi: UsersApiService,
    private http: HttpClient) { }

  fetchPlaylistById(playlist_id: string): Observable<Playlist> {
    return this.http.get<Playlist>(this.api_url + '/playlists/' + playlist_id)
  }

  fetchPlaylists(): Observable<Playlist[]> {
    return this.http.get<PagingObject<Playlist>>(this.api_url + '/me/playlists').pipe(
      map(resp => resp.items)
    )
  }

  deletePlaylist(playlist_id: string) {
    return this.http.delete(`${this.api_url}/playlists/${playlist_id}/followers`)
  }

  savePlaylist(draft: Playlist): Observable<Playlist> {
    const payloadPlaylistData = {
      name: draft.name,
      public: draft.public,
      description: draft.description
    };

    return of(draft).pipe(
      exhaustMap(draft => iif(() => !!draft.id,

        this.http.put<Playlist>(
          this.api_url + '/playlists/' + draft.id, payloadPlaylistData)
          .pipe(mapTo(draft)),

        this.userApi.getCurrentUser().pipe(exhaustMap(user => {
          if (!user) { throw 'Not logged in ' }

          return this.http.post<Playlist>(
            this.api_url + '/users/' + user.id + '/playlists', payloadPlaylistData)
        }))
      ))
    );

    // if (draft.id) {
    //   return this.http.put<Playlist>(
    //     this.api_url + '/playlists/' + draft.id, payloadPlaylistData)
    //     .pipe(mapTo(draft))
    // } else {
    //   return this.userApi.getCurrentUser().pipe(
    //     exhaustMap(user => {
    //       return this.http.post<Playlist>(this.api_url + '/users/' + user.id + '/playlists', payloadPlaylistData)
    //     })
    //   )
    // }
  }
}
