import { AuthConfig } from "angular-oauth2-oidc";

export const authTokenFlowConfig: AuthConfig = {
  oidc:false,

  // Url of the Identity Provider
  // issuer: 'https://idsvr4.azurewebsites.net',
  loginUrl: 'https://accounts.spotify.com/authorize',

  // URL of the SPA to redirect the user after silent refresh
  silentRefreshRedirectUri: window.location.origin + '/assets/silent-refresh.html',

  // defaults to true for implicit flow and false for code flow
  // as for code code the default is using a refresh_token
  // Also see docs section 'Token Refresh'
  useSilentRefresh: true,

  // URL of the SPA to redirect the user to after login
  redirectUri: window.location.origin + '/index.html',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  // clientId: 'server.code',
  clientId: '4845f776605345db905813e5eb183ff0',

  customQueryParams:{
    // show_dialog:'true',
  },

  // Just needed if your auth server demands a secret. In general, this
  // is a sign that the auth server is not configured with SPAs in mind
  // and it might not enforce further best practices vital for security
  // such applications.
  // dummyClientSecret: 'secret',

  responseType: 'token',

  // set the scope for the permissions the client should request
  // The first four are defined by OIDC.
  // Important: Request offline_access to get a refresh token
  // The api scope is a usecase specific one
  // scope: 'openid profile email offline_access api',

  // https://developer.spotify.com/documentation/general/guides/scopes/
  scope: 'playlist-modify-private playlist-read-private playlist-modify-public playlist-read-collaborative',

  showDebugInformation: true,
};

export const environment = {
  production: true,
  
  authConfig: authTokenFlowConfig,
  api_url: 'https://api.spotify.com/v1'
};